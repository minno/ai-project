#pragma once

#include <SFML/System/Vector2.hpp>

#include <cmath>

typedef sf::Vector2f vec2;

const vec2 zero {0, 0};

const static float pi = 3.14159265358979;

template<typename T>
inline T clamped(T value, T low, T high) {
	if (value < low) {
		return low;
	}
	if (value > high) {
		return high;
	}
	return value;
}

template<typename T>
inline T clamped_within(T value, T goal, T tolerance) {
	return clamped(value, goal-tolerance, goal+tolerance);
}

inline float dot(vec2 a, vec2 b) {
	return a.x * b.x + a.y * b.y;
}

inline float magnitude(vec2 a) {
	return std::sqrt(dot(a, a));
}

inline float angle(vec2 a) {
	return std::atan2(a.y, a.x);
}

inline float angle_diff(vec2 v1, vec2 v2) {
	float angle1 = angle(v1);
	float angle2 = angle(v2);
	float diff = angle2 - angle1;
	if (diff > pi) {
		diff -= 2*pi;
	} else if (diff < -pi) {
		diff += 2*pi;
	}
	return diff;
}

inline float distance(vec2 a, vec2 b) {
	return magnitude(a - b);
}

inline vec2 scaled(vec2 a, float new_magnitude) {
	return a / magnitude(a) * new_magnitude;
}

inline vec2 normalized(vec2 a) {
	return scaled(a, 1);
}

inline vec2 rotated(vec2 a, float new_angle) {
	return vec2{std::cos(new_angle), std::sin(new_angle)} * magnitude(a);
}

template<typename T>
T interpolate(float fraction, T start, T end) {
	return fraction * end + (1 - fraction) * start;
}
