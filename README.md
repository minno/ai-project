This is a project made by Will Fischer and Sam Torzewski for Prof. Baylor's CSCI 4511W class at the University of Minnesota Twin Cities. To build it you need G++ 4.7 or later or equivalent (needs full C++11 support minus some libraries) and SFML 2.0 or higher.

http://sfml-dev.org/

This project is licensed under the DWTFYWTPL:

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.
