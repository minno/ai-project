#pragma once

#include "pathway.h"
#include "vehicleparams.h"

#include "../vec2.hpp"
#include "../lookupcontainer.hpp"

#include <cmath>

class Vehicle {
public:

	Vehicle(vec2 position, vec2 velocity, id_t params_id)
		: position_(position), velocity_(velocity), params_(vparams.lookup(params_id)), params_id_(params_id)
		{}

	void update(float dt) {
		position_ += velocity_ * dt;

		// TODO: update state based on steering behaviors
		update_velocity(dt);
//		if (curr_following_path_) {
//			velocity_ += steer_to_follow_path(*curr_following_path_, 1, 1);
//		}
	}
	void update_params(){
		params_ = vparams.lookup(params_id_);
	}

	vec2 steer_to_seek(vec2 goal) const;
	vec2 steer_to_follow_path(const PolyPath& path, float prediction_time) const;
	vec2 steer_to_maintain_speed() const;
	vec2 steer_to_avoid_collisions() const;

	// Returns the desired update to the current velocity vector
	vec2 steer() const {
		vec2 path_follow_desired = (curr_following_path_ ? steer_to_follow_path(*curr_following_path_, 1) : zero);
		vec2 maintain_speed_desired = steer_to_maintain_speed();
		vec2 collision_avoid_desired = steer_to_avoid_collisions();
		return path_follow_desired * params_.steer.path_follow_weight +
			maintain_speed_desired * params_.steer.maintain_speed_weight +
			collision_avoid_desired * params_.steer.collision_avoid_weight;
	}

	// Changes the vehicle's speed to get as close as possible to the desired steering,
	// taking max speed and stuff into account.
	void update_velocity(float dt) {
		params_ = vparams.lookup(params_id_);
		auto desired_velocity = steer() + velocity_;

		if (dot(desired_velocity, velocity_) < 0) {
			// Target is behind, slow down.
			desired_velocity *= 0.00000001f;
		}

		float new_speed = clamped_within(magnitude(desired_velocity), magnitude(velocity_), params_.drive.max_acceleration*dt);
		desired_velocity = scaled(desired_velocity, new_speed);

		float angle_change = angle_diff(velocity_, desired_velocity);
		if (std::abs(angle_change) > params_.drive.max_turn_speed*dt) {
			int sign = (angle_change > 0 ? 1 : -1);
			float clamped_angle_change = params_.drive.max_turn_speed*dt * sign;
			float clamped_angle = clamped_angle_change + angle(velocity_);
			desired_velocity = rotated(desired_velocity, clamped_angle);
		}

		desired_velocity = scaled(desired_velocity, clamped(magnitude(desired_velocity), 0.0f, params_.drive.max_speed));

		velocity_ = desired_velocity;
	};

	void set_position(vec2 new_position) {
		position_ = new_position;
	}

	vec2 get_position() const {
		return position_;
	}

	vec2 get_velocity() const {
		return velocity_;
	}

	id_t get_params_id() const{
		return params_id_;
	}

	void set_path(const PolyPath* new_path) {
		curr_following_path_ = new_path;
	}

	void set_params(id_t params_id) {
		params_ = vparams.lookup(params_id);
	}

	void set_params(const vehicle_params& new_params) {
		params_ = new_params;
	}

private:
	// Current state
	vec2 position_;
	vec2 velocity_;

	vehicle_params params_;
	id_t params_id_;

	const static int radius_ = 13;

	// Steering parameters
	const PolyPath* curr_following_path_ = nullptr;
};

extern LookupContainer<Vehicle> vehicles;
