#pragma once

#include "../lookupcontainer.hpp"

struct steering_params {
	float path_follow_weight;
	float collision_avoid_weight;
	float maintain_speed_weight;
};

struct driving_params {
	float max_speed;
	float max_acceleration;
	float max_turn_speed;
	int direction;
};

struct vehicle_params {
	steering_params steer;
	driving_params drive;
};

extern LookupContainer<vehicle_params> vparams;

const static vehicle_params default_behavior = {
	{2.0, 2.0, 1.0},
	{50.0, 10.0, 1.0, 1}
};

const static vehicle_params greedy_behavior = {
	{1.0, 0, 2.0},
	{50.0, 10.0, 1.0, -1}
};

const static vehicle_params fast_behavior = {
	{1.0, 2.0, 4.0},
	{200.0, 10.0, 1.0, 1}
};

const static vehicle_params accel_behavior = {
	{2.0, 3.0, 1.0},
	{200.0, 50.0, 1.0, -1}
};

const static vehicle_params follower_behavior = {
	{10.0, 0, 2.0},
	{50.0, 10.0, 1.0, 1}
};

const static vehicle_params ignorant_behavior = {
	{0.0, 5.0, 2.0},
	{50.0, 10.0, 1.0, -1}
};

const static vehicle_params fast_turner_behavior = {
	{1.0, 1.0, 2.0},
	{50.0, 10.0, 3.0, 1}
};

const static vehicle_params slow_behavior = {
	{5.0, 0.0, 5.0},
	{20.0, 10.0, 2.0, -1}
};

const static vehicle_params slow_accel_behavior = {
	{2.0, 2.0, 3.0},
	{300.0, 1.0, 2.0, 1}
};

const static vehicle_params maintain_behavior = {
	{5.0, 2.0, 15.0},
	{200.0, 10.0, 5.0, -1}
};
