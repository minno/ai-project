#pragma once

#include <vector>
#include <type_traits>
#include <iostream>

#include <string>
#include <fstream>
#include <sstream>

#include <stdlib.h>

#include "../vec2.hpp"

class PolyPath {
public:
	struct pathSegment {
		vec2 origin;
		vec2 displacement;

		bool operator==(const pathSegment& other) const {
			return origin == other.origin && displacement == other.displacement;
		}

		bool operator!=(const pathSegment& other) const {
			return !(*this == other);
		}
	};

	PolyPath() = delete;
	bool no_pts;

	template<typename PointsContainer>
	PolyPath(const PointsContainer& points, float radius, bool cyclic = false)
		: radius_(radius), cyclic_(cyclic) {
		no_pts = true;
		static_assert(std::is_same<decltype(*begin(points)), const vec2&>::value,
					  "Parameter points must be a container of vec2 objects.");

		for (vec2 point : points) {
			//path.push_back({point, zero});
			no_pts = false;
			add_point_to_path(point);
		}
		if(no_pts){
			add_point_to_path({300,300});
		}
	}

	PolyPath(const PolyPath& other) = delete;
	PolyPath(PolyPath&& other) = delete;
	~PolyPath() = default;

	float get_path_length() const {
		return total_path_length_;
	}


	float get_radius() const {
		return radius_;
	}

	const std::vector<pathSegment>& get_path() const {
		return path;
	}

	// Returns the closest path segment.
	PolyPath::pathSegment nearest_segment(vec2 point) const;
	// Returns the closest point to "point" that is on the path.
	vec2 nearest_point_on_path(vec2 point) const;
	// Returns the distance to the closest point on the path.
	float distance_to_path(vec2 point) const;
	bool is_in_path(vec2 point) const;

	// Returns the distance along the path from the start to the closest point to "point".
	float distance_along_path(vec2 point) const;
	vec2 point_from_distance(float dist) const;

	//Returns the segment at the end of the path
	PolyPath::pathSegment last_segment() const{
		return path.back();
	}
	void add_point_to_path(vec2 point);
	bool is_cyclic(){
		return cyclic_;
	}

private:
	std::vector<pathSegment> path;
	float radius_;
	bool cyclic_;

	float total_path_length_ = 0;

	//template<typename PointsContainer>
	//void concatenate(const PointsContainer& points);
};

vec2 nearest_point_on_segment(vec2 point, PolyPath::pathSegment segment);
