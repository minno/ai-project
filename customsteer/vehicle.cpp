#include "vehicle.h"

LookupContainer<Vehicle> vehicles;

#include <iostream>

vec2 Vehicle::steer_to_seek(vec2 goal) const {
	vec2 goal_velocity = goal - position_;
//	if (magnitude(goal_velocity) > max_speed_) {
//		goal_velocity *= max_speed_ / magnitude(goal_velocity);
//	}
	return goal_velocity - velocity_;
}

vec2 Vehicle::steer_to_follow_path(const PolyPath& path, float prediction_time) const {
	vec2 future_position = position_ + prediction_time * velocity_;
	float future_distance_from_path = path.distance_to_path(future_position);

	float curr_distance_along_path = path.distance_along_path(position_);
	float future_distance_along_path = path.distance_along_path(future_position);

	float max_movement = magnitude(velocity_) * prediction_time;

	bool right_way;
	if (params_.drive.direction > 0) {
		// Wraparound detection: the vehicle can't move more than max_distance, so if the distance
		// along the path jumps by more than max_distance, it was because of crossing the boundary
		// in the desired direction.
		if (curr_distance_along_path < future_distance_along_path ||
			curr_distance_along_path - future_distance_along_path > max_movement) {
			right_way = true;
		} else {
			right_way = false;
		}
	} else {
		// Wraparound detection: the vehicle can't move more than max_distance, so if the distance
		// along the path jumps by more than max_distance, it was because of crossing the boundary
		// in the desired direction.
		if (curr_distance_along_path > future_distance_along_path ||
			future_distance_along_path - curr_distance_along_path > max_movement) {
			right_way = true;
		} else {
			right_way = false;
		}
	}

	if (future_distance_from_path < path.get_radius() && right_way) {
		return vec2{0, 0}; // No change required.
	} else {
	float goal_distance_along_path = curr_distance_along_path + max_movement * params_.drive.direction;
	vec2 goal_point = path.point_from_distance(goal_distance_along_path);

	return steer_to_seek(goal_point);
	}
}

vec2 Vehicle::steer_to_maintain_speed() const {
	vec2 goal_velocity = scaled(velocity_, params_.drive.max_speed);
	return goal_velocity - velocity_;
}

vec2 Vehicle::steer_to_avoid_collisions() const {
	const auto& other_vehicles = vehicles;

	vec2 total_avoidance {0, 0};

	for (const auto& id_vehicle_pair : other_vehicles) {
		const auto& v = id_vehicle_pair.second;
		if (&v == this) {
			// Running from myself is rather ineffective and results in a disturbing number of NaNs.
			continue;
		}
		vec2 displacement = v.get_position() - position_;
		float dist = magnitude(displacement);
		vec2 rel_velocity = v.get_velocity() - velocity_;
		float approach_speed = -dot(normalized(displacement), rel_velocity);
		if (approach_speed <= 0) {
			// Not running into this guy anytime soon.
			continue;
		}
		float time_to_collision = (dist - 2*radius_) / approach_speed;
		if (time_to_collision < 1.0/60) {
			// GTFO
			total_avoidance = -scaled(displacement, 10000);
			return total_avoidance;
		}
		if (time_to_collision > 2) {
			// DGAF
			continue;
		}
		total_avoidance += -scaled(displacement, 50/time_to_collision*time_to_collision);
	}
	return total_avoidance;
}
