#include "pathway.h"

#include <algorithm>
#include <cassert>

vec2 nearest_point_on_segment(vec2 point, PolyPath::pathSegment segment) {
	auto local_point = point - segment.origin;
	float projection_magnitude = dot(local_point, normalized(segment.displacement));

	if (projection_magnitude < 0) {
		return segment.origin;
	} else if (projection_magnitude > magnitude(segment.displacement)) {
		return segment.origin + segment.displacement;
	} else {
		return interpolate(
			projection_magnitude / magnitude(segment.displacement),
			segment.origin,
			segment.origin + segment.displacement);
	}
}

float distance_to_segment(vec2 point, PolyPath::pathSegment segment) {
	return distance(point, nearest_point_on_segment(point, segment));
}

PolyPath::pathSegment PolyPath::nearest_segment(vec2 point) const {
	auto closest_segment = std::min_element(begin(path), end(path),
		[=](const pathSegment& seg1, const pathSegment& seg2) {
			return distance_to_segment(point, seg1) < distance_to_segment(point, seg2);
		});
	return *closest_segment;
}

vec2 PolyPath::nearest_point_on_path(vec2 point) const {
	return nearest_point_on_segment(point, nearest_segment(point));
}

float PolyPath::distance_to_path(vec2 point) const {
	return distance(point, nearest_point_on_path(point));
}

bool PolyPath::is_in_path(vec2 point) const {
	return distance_to_path(point) <= radius_;
}

float PolyPath::distance_along_path(vec2 point) const {
	auto segment = nearest_segment(point);
	auto path_point = nearest_point_on_path(point);
	float total_distance = 0;
	for (auto it = begin(path); *it != segment; ++it) {
		total_distance += magnitude(it->displacement);
	}
	total_distance += distance(path_point, segment.origin);
	return total_distance;
}

vec2 PolyPath::point_from_distance(float dist) const {
	// Handle distances that aren't in the range [0, path_len)
	if (cyclic_) {
		while (dist < 0) {
			dist += total_path_length_;
		}
		while (dist >= total_path_length_) {
			dist -= total_path_length_;
		}
	} else {
		if (dist < 0) {
			return path.front().origin;
		} else if (dist >= total_path_length_) {
			return path.back().origin + path.back().displacement;
		}
	}

	assert(dist >= 0 && dist < total_path_length_);

	for (auto segment : path) {
		if (dist < magnitude(segment.displacement)) {
			return interpolate(
				dist / magnitude(segment.displacement),
				segment.origin,
				segment.origin + segment.displacement);
		} else {
			dist -= magnitude(segment.displacement);
		}
	}

	// Should be unreachable, but better safe than sorry.
	return vec2{0, 0};
}

void PolyPath::add_point_to_path(vec2 point){
	int s = path.size();
	//path.push_back({point, zero});
	if(s == 0){
		path.push_back({point, zero});
		path.back().displacement = {0, 0.005};
		total_path_length_ += magnitude(path.back().displacement);
	}
	else if(s == 1 && no_pts){
		total_path_length_ -= magnitude(path.back().displacement);
		path.push_back({point, zero});
		path.back().displacement = {0, 0.005};
		total_path_length_ += magnitude(path.back().displacement);
	}
	else if(cyclic_){
			total_path_length_ -= magnitude(path.back().displacement);
			path.back().displacement = point - path.back().origin;
			if(path.back().displacement == zero){
				path.back().displacement = {0, 0.005};
			}
			total_path_length_ += magnitude(path.back().displacement);

			path.push_back({point, zero});
			path.back().displacement = path.front().origin - path.back().origin;
			if(path.back().displacement == zero){
				path.back().displacement = {0, 0.005};
			}
			total_path_length_ += magnitude(path.back().displacement);

	}
	else{
		if(total_path_length_ >= 0.01){
			vec2 temp_point = path.back().origin + path.back().displacement;
			path.push_back({temp_point, zero});
		}
		else{
			total_path_length_ -= magnitude(path.back().displacement);
		}
		path.back().displacement = point - path.back().origin;
		if(path.back().displacement == zero){
				path.back().displacement = {0, 0.005};
		}
		total_path_length_ += magnitude(path.back().displacement);
	}
}


//template<typename PointsContainer>
//void PolyPath::concatenate(const PointsContainer& points){
//	int start_position = path.size();
//	if(start_position != 0){
//			start_position--;
//	} //We want to change the displacement of the last object in the current path, too.
//
//	static_assert(std::is_same<decltype(*begin(points)), const vec2&>::value,
//					  "Parameter points must be a container of vec2 objects.");
//	for (vec2 point : points) {
//		path.push_back({point, zero});
//	}
//
//	for (int i = start_position; i < path.size() - 1; ++i) {
//		path[i].displacement = path[i+1].origin - path[i].origin;
//		total_path_length_ += magnitude(path[i].displacement);
//	}
//
//	if (cyclic_) {
//		path.back().displacement = path.front().origin - path.back().origin;
//		total_path_length_ += magnitude(path.back().displacement);
//	} else {
//		path.pop_back();
//	}
//}
