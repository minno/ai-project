#pragma once

#include <unordered_map>

typedef unsigned int id_t;

template<typename T>
class LookupContainer {
public:
	typedef typename std::unordered_map<id_t, T>::iterator       iterator;
	typedef typename std::unordered_map<id_t, T>::const_iterator const_iterator;

	id_t insert(const T& val) {
		id_t this_id = id_counter++;
		container.insert(std::make_pair(this_id, val));
		return this_id;
	}

	template<typename... Types>
	id_t emplace(Types&&... args) {
	 	id_t this_id = id_counter++;
		container.insert(std::make_pair(this_id, T{args...}));
		return this_id;
	}

	size_t size() {
		return container.size();
	}

	void remove(id_t id) {
		container.erase(id);
	}

	T& lookup(id_t id) {
		return container.at(id);
	}

	const T& lookup(id_t id) const {
		return container.at(id);
	}

	bool contains(id_t id) const {
		return container.find(id) != container.end();
	}

	iterator begin() {
		return container.begin();
	}

	const_iterator begin() const {
		return container.begin();
	}

	iterator end() {
		return container.end();
	}

	const_iterator end() const {
		return container.end();
	}

private:
	std::unordered_map<id_t, T> container;
	static id_t id_counter;
};

template<typename T>
id_t LookupContainer<T>::id_counter = 0;
