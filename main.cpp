#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include "customsteer/pathway.h"
#include "customsteer/vehicle.h"

#include "drawables/renderedpolypath.h"
#include "drawables/renderedvehicle.h"

#include "lookupcontainer.hpp"

#include "loader.hpp"

#include "options.hpp"

#include <iostream>
#include <fstream>
#include <string>
#include <random>
#include <vector>
#include <chrono>
#include <list>

std::mt19937 rand_gen(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count()); // That's a mouthful
std::uniform_real_distribution<float> dist {0, 1};
auto rand_float = [&](){return dist(rand_gen);};

int main() {
	sf::Font font;
	font.loadFromFile("comic.ttf");
    sf::Clock clock;

    sf::RenderWindow scene_window(sf::VideoMode(900, 600), "Steering");
	scene_window.setPosition(sf::Vector2i(50.f, 50.f));


	int path_num;
	std::string input_string;
	std::ifstream file_stream("input_files/file_reader.txt");
	file_stream >> path_num;
	getline(file_stream, input_string);
	for(int i = 0; i < path_num; i++){
		getline(file_stream, input_string);
	}
	std::string load_string = "input_files/";
	load_string.append(input_string);
	scene_window.setTitle(input_string);
	Loader loader(load_string.c_str());
    // A circle centered on (300, 300) with radius 200: input_files/default_circle.txt
	PolyPath path (loader.points, loader.radius, loader.cycle);
	RenderedPolyPath drawn_path (path);
	std::vector<RenderedVehicle> drawn_vehicles;

	id_t default_behavior_id = vparams.insert(default_behavior);
	id_t greedy_behavior_id = vparams.insert(greedy_behavior);
	id_t fast_behavior_id = vparams.insert(fast_behavior);
	id_t accel_behavior_id = vparams.insert(accel_behavior);
	id_t follower_behavior_id = vparams.insert(follower_behavior);
	id_t ignorant_behavior_id = vparams.insert(ignorant_behavior);
	id_t fast_turner_behavior_id = vparams.insert(fast_turner_behavior);
	id_t slow_behavior_id = vparams.insert(slow_behavior);
	id_t slow_accel_behavior_id = vparams.insert(slow_accel_behavior);
	id_t maintain_behavior_id = vparams.insert(maintain_behavior);
	Options options(default_behavior_id);

    while (scene_window.isOpen()) {
		//bool update_params = false;
        sf::Event event;
        while (scene_window.pollEvent(event)) {
            switch (event.type) {
			case sf::Event::Closed:
				scene_window.close();
				break;
			case sf::Event::TextEntered:
//					if (event.text.unicode == 'c'){
//						id_t curr_param_id = options.current_id;
//						std::for_each(std::begin(vehicles), std::end(vehicles),
//						[&](std::pair<const id_t, Vehicle>& v){
//                              vehicles.remove(v.first);
//						});
//						drawn_vehicles.clear();
//
//					}
				    if (event.text.unicode < 128)
					{
						char input = static_cast<char>(event.text.unicode);
						options.handle_event(input);
					}
//				options.handle_event();
//				break;
			case sf::Event::MouseButtonPressed:
				if (event.mouseButton.button == sf::Mouse::Button::Left) {
					vec2 new_pos {
						static_cast<float>(event.mouseButton.x),
						static_cast<float>(event.mouseButton.y)};
					vec2 new_velocity {1, 0};
					int curr_param_id = options.current_id;
					id_t id = vehicles.emplace(new_pos, new_velocity, options.current_id);
					vehicles.lookup(id).set_path(&path);
					drawn_vehicles.emplace_back(vehicles, id);
				} else if (event.mouseButton.button == sf::Mouse::Button::Right) {
					vec2 new_pos {
						static_cast<float>(event.mouseButton.x),
						static_cast<float>(event.mouseButton.y)};
//					vec2 new_velocity {1, 0};
//					id_t id = vehicles.emplace(new_pos, new_velocity, greedy_behavior_id);
//					vehicles.lookup(id).set_path(&path);
//					drawn_vehicles.emplace_back(vehicles, id);
                    path.add_point_to_path(new_pos);
                    drawn_path.append(path, path.last_segment(), path.is_cyclic());
                    //vparams.lookup(default_behavior_id).drive.direction *= -1;

				}
				break;
			default:
				// ignore it
				break;
            }
        }

        float dt = clock.restart().asSeconds();
//        if(update_params){
//			std::for_each(std::begin(vehicles), std::end(vehicles),
//				[&](std::pair<const id_t, Vehicle>& v){ v.second.update_params();});
//
//        }

		std::for_each(std::begin(vehicles), std::end(vehicles),
			[&](std::pair<const id_t, Vehicle>& v){ v.second.update(dt); });

		scene_window.clear(sf::Color{0, 200, 33});
		scene_window.draw(drawn_path);
		std::for_each(begin(drawn_vehicles), end(drawn_vehicles),
			[&](RenderedVehicle& v){ scene_window.draw(v); });

		scene_window.draw(options);
		scene_window.display();
    }

    return 0;
}
