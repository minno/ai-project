#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <string>
#include <sstream>

enum OptionsState {def, p_follow, c_avoid, maintain, speed_m, accel_m, turn_m, dir, no_option};

class Options : public sf::Drawable{
	enum InputState {no_input, num, p_entry, enter, b_space, esc, flip_dir, clear_group};
public:

	sf::Font font;
    //steering_params
    sf::Text path_following;
    sf::Text collision_avoid;
    sf::Text maintain_speed;
    //driving_params
    sf::Text max_speed;
    sf::Text max_acceleration;
    sf::Text max_turn_speed;
    sf::Text direction;

	id_t current_id;
	bool clear_first;
	OptionsState options_state;
	InputState input_state;
	sf::Color default_color;

    Options(id_t id){
    	default_color = {0,0,0};
    	clear_first = false;
    	current_id = id;
    	options_state = def;
    	input_state = no_input;
    	font.loadFromFile("arial.ttf");
    	initialize_text(id_label, "Group Number 0", font, 20, 600, 20 );
    		 //steering_params labels
		initialize_text(path_following_label, "[p] - path following", font, 16, 600, 60);
		initialize_text(collision_avoid_label, "[c] - collision avoid", font, 16, 600, 90);
		initialize_text(maintain_speed_label, "[m] - maintain speed", font, 16, 600, 120);
		initialize_text(max_speed_label, "[s] - speed limit", font, 16, 600, 150);
		initialize_text(max_acceleration_label, "[a] - acceleration limit", font, 16, 600, 180);
		initialize_text(max_turn_speed_label, "[t] - turn speed limit", font, 16, 600, 210);
		initialize_text(direction_label, "[d] - direction", font, 16, 600, 240);
		//initialize_text(clear_group_label, "[c] - clear group", font, 16, 600, 300);


		float a_float;
		a_float = vparams.lookup(current_id).steer.path_follow_weight;
		initialize_text(path_following, a_float, font, 16, 800, 60);

		a_float = vparams.lookup(current_id).steer.collision_avoid_weight;
		initialize_text(collision_avoid, a_float, font, 16, 800, 90);

		a_float = vparams.lookup(current_id).steer.maintain_speed_weight;
		initialize_text(maintain_speed, a_float, font, 16, 800, 120);

		a_float = vparams.lookup(current_id).drive.max_speed;
		initialize_text(max_speed, a_float, font, 16, 800, 150);

		a_float =  vparams.lookup(current_id).drive.max_acceleration;
		initialize_text(max_acceleration, a_float, font, 16, 800, 180);

		a_float =  vparams.lookup(current_id).drive.max_turn_speed;
		initialize_text(max_turn_speed, a_float, font, 16, 800, 210);

		a_float = vparams.lookup(current_id).drive.direction;
		if(a_float < 0){
			initialize_text(direction, "backward", font, 16, 800, 240);
		}
		else initialize_text(direction, "forward", font, 16, 800, 240);

    }
	void change_group(){
		std::string str = id_label.getString();
		str.resize (str.size () - 1);

		str += (current_id + '0');
		id_label.setString(str);
		float a_float;

		a_float = vparams.lookup(current_id).steer.path_follow_weight;
		set_text_as_float(path_following, a_float);

		a_float = vparams.lookup(current_id).steer.collision_avoid_weight;
		set_text_as_float(collision_avoid, a_float);

		a_float = vparams.lookup(current_id).steer.maintain_speed_weight;
		set_text_as_float(maintain_speed, a_float);

		a_float = vparams.lookup(current_id).drive.max_speed;
		set_text_as_float(max_speed,a_float);

		a_float =  vparams.lookup(current_id).drive.max_acceleration;
		set_text_as_float(max_acceleration, a_float);

		a_float =  vparams.lookup(current_id).drive.max_turn_speed;
		set_text_as_float(max_turn_speed, a_float);

		a_float = vparams.lookup(current_id).drive.direction;
		if(a_float < 0){
			direction.setString("backward");
		}
		else {
			direction.setString("forward");
		}

	}
	bool handle_event(char input){
		//input setup
		if ((input <= '9' && input >= '0')|| input == '.'){
			input_state = num;
		}
		else if(input == 13){
			input_state = enter;
		}
		else if(input == 8){
			input_state = b_space;
		}
		else if(input == 27){
			input_state = esc;
		}
		else if(input == 'p' || input == 'c' || input == 'm' ||
				input == 's' || input == 'a' || input == 't'){
			input_state = p_entry;

		}
		else if(input == 'd'){
			input_state = flip_dir;
		}
//		else if(input == 'c'){
//			input_state = clear_group;
//		}
		//running the state machine
		if(options_state == def){
			def_event(input);
		}
		else{
			handle_params(input);
		}

		input_state = no_input;
		return true;
	}
	void handle_params(char input){
		sf::Text* text;
		sf::Text* label;
		if(options_state == p_follow){
			text = &path_following;
			label = &path_following_label;
		}
		else if (options_state == c_avoid){
			text = &collision_avoid;
			label = &collision_avoid_label;
		}
		else if (options_state == maintain){
			text = &maintain_speed;
			label = &maintain_speed_label;
		}
		else if (options_state == speed_m){
			text= &max_speed;
			label = &max_speed_label;
		}
		else if (options_state == accel_m){
			text = &max_acceleration;
			label = &max_acceleration_label;
		}
		else if (options_state == turn_m){
			text = &max_turn_speed;
			label = &max_turn_speed_label;
		}

		if(input_state == esc){
			return_to_default(input,*text,*label,true);
		}
		else if(input_state == p_entry || input_state == enter){
			return_to_default(input, *text, *label);
		}
		else if(input_state == b_space || input_state == num){
			std::string str = text->getString();
			if(clear_first){
				str.clear();
				clear_first = false;
			}
			if(input_state == b_space && str.size() > 0){
				str.resize (str.size () - 1);
			}
			else if(input_state == num){
				str += input;
			}
			text->setString(str);
		}
	}
	void def_event(char input){
		if(input_state == num && input != '.'){
			current_id = input - '0';
			change_group();
		}
		else if (input_state == flip_dir){
			std::string str = direction.getString();
			if(str == "backward"){
				str = "forward";
			}
			else {
				str = "backward";
			}
			direction.setString(str);
			vparams.lookup(current_id).drive.direction *= -1;
		}

		else if (input_state == p_entry){
			clear_first = true;
			if(input == 'p'){
				options_state = p_follow;
				set_state(path_following, path_following_label);
			}
			else if (input == 'c'){
				options_state = c_avoid;
				set_state(collision_avoid, collision_avoid_label);
			}
			else if (input == 'm'){
				options_state = maintain;
				set_state(maintain_speed, maintain_speed_label);
			}
			else if (input == 's'){
				options_state = speed_m;
				set_state(max_speed, max_speed_label);
			}
			else if (input == 'a'){
				options_state = accel_m;
				set_state(max_acceleration, max_acceleration_label);
			}
			else if (input == 't'){
				options_state = turn_m;
				set_state(max_turn_speed, max_turn_speed_label);
			}

		}
	}
	void set_state(sf::Text& text, sf::Text& label){
		text.setColor({100,0,250});
		label.setColor({100,0,250});

	}
	void return_to_default(char input, sf::Text& text, sf::Text& label, bool input_cancel = false){
		bool cancel = input_cancel;
		text.setColor(default_color);
		label.setColor(default_color);

		std::stringstream string_stream;
		std::string a_string;
		std::string test_string;
		test_string = text.getString();
		if(test_string.empty()){
				cancel = true;
		}
		if (cancel){
			if(options_state == p_follow){
				string_stream << vparams.lookup(current_id).steer.path_follow_weight;
			}
			else if (options_state == c_avoid){
				string_stream << vparams.lookup(current_id).steer.collision_avoid_weight;
			}
			else if (options_state == maintain){
				string_stream << vparams.lookup(current_id).steer.maintain_speed_weight;
			}
			else if (options_state == speed_m){
				string_stream << vparams.lookup(current_id).drive.max_speed;
			}
			else if (options_state == accel_m){
				string_stream << vparams.lookup(current_id).drive.max_acceleration;
			}
			else if (options_state == turn_m){
				string_stream << vparams.lookup(current_id).drive.max_turn_speed;
			}
			string_stream >> a_string;
			text.setString(a_string);
		}
		else {

			string_stream << test_string;
			if(options_state == p_follow){
				string_stream >> vparams.lookup(current_id).steer.path_follow_weight;
			}
			else if (options_state == c_avoid){
				string_stream >> vparams.lookup(current_id).steer.collision_avoid_weight;
			}
			else if (options_state == maintain){
				string_stream >> vparams.lookup(current_id).steer.maintain_speed_weight;
			}
			else if (options_state == speed_m){
				string_stream >> vparams.lookup(current_id).drive.max_speed;
			}
			else if (options_state == accel_m){
				string_stream >> vparams.lookup(current_id).drive.max_acceleration;
			}
			else if (options_state == turn_m){
				string_stream >> vparams.lookup(current_id).drive.max_turn_speed;
			}

		}

		options_state = def;
		def_event(input);
	}

    void draw(sf::RenderTarget& target, sf::RenderStates states) const{
		target.draw(path_following);
		target.draw(collision_avoid);
		target.draw(maintain_speed);
		target.draw(max_speed);
		target.draw(max_acceleration);
		target.draw(max_turn_speed);
		target.draw(direction);

		target.draw(path_following_label);
		target.draw(collision_avoid_label);
		target.draw(maintain_speed_label);
		target.draw(max_speed_label);
		target.draw(max_acceleration_label);
		target.draw(max_turn_speed_label);
		target.draw(direction_label);

		target.draw(id_label);

    }
private:

	void initialize_text(sf::Text& text, std::string the_string, sf::Font& font, int fontsize, float pos_x, float pos_y){
		text.setFont(font);
		text.setColor(default_color);
		text.setPosition(pos_x, pos_y);
		text.setCharacterSize(fontsize);
		text.setString(the_string);

	}
	void initialize_text(sf::Text& text, float input, sf::Font& font, int fontsize, float pos_x, float pos_y){
		std::string a_string;
		std::stringstream string_stream;
		string_stream << input;
		string_stream >> a_string;
		initialize_text(text, a_string, font, fontsize, pos_x, pos_y);
	}
	void set_text_as_float(sf::Text& text, float input){
		std::string a_string;
		std::stringstream string_stream;
		string_stream << input;
		string_stream >> a_string;
		text.setString(a_string);
	}

	sf::Text id_label;
	//steering_params labels
    sf::Text path_following_label;
    sf::Text collision_avoid_label;
    sf::Text maintain_speed_label;
    //driving_params labels
    sf::Text max_speed_label;
    sf::Text max_acceleration_label;
    sf::Text max_turn_speed_label;
    sf::Text direction_label;
    sf::Text clear_group_label;
};
