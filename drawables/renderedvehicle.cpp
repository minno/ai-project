#include "renderedvehicle.h"

std::shared_ptr<sf::Texture> RenderedVehicle::texture_ = nullptr;

sf::Color RenderedVehicle::colorLookup(int lookup_val){
	sf::Color color(255,255,255);

	int num_colors = 13;
	switch(lookup_val){

		case 0: color = sf::Color(255,255,255);
				break;
		case 1: color = sf::Color(0,0,255);
				break;
		case 2: color = sf::Color(255,0,0);
				break;
		case 3: color = sf::Color(255,255,0);
				break;
		case 4: color = sf::Color(0,255,255);
				break;
		case 5: color = sf::Color(255,0,255);
				break;
		case 6:	color = sf::Color(0,255,0);
				break;
		case 7: color = sf::Color(255,128,0);
				break;
		case 8: color = sf::Color(128,0,255);
				break;
		case 9: color = sf::Color(128,128,128);
				break;
		case 10: color = sf::Color(0,128,255);
				break;
		case 11: color = sf::Color(128,255,0);
				break;
		case 12: color = sf::Color(0,255,128);
				break;
		default:
			sf::Color temp_color = colorLookup(lookup_val - num_colors);
			color.r -= 0.8*temp_color.r;
			color.g -= 0.8*temp_color.g;
			color.b -= 0.8*temp_color.b;
				break;
	}
	return color;
}
