#pragma once

#include "../customsteer/vehicle.h"

#include "../vec2.hpp"
#include "../lookupcontainer.hpp"

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Texture.hpp>

#include <cmath>
#include <memory>

class RenderedVehicle : public sf::Drawable
{
public:
	RenderedVehicle(const LookupContainer<Vehicle>& container, id_t id)
		: vehicles_(container), vehicle_id_(id) {
		rect_.setSize(sf::Vector2f{26, 12});
		rect_.setOrigin(rect_.getSize() / 2.0f);
		params_id_ = vehicles_.lookup(id).get_params_id();
		rect_.setFillColor(colorLookup(params_id_));
		if (!texture_) {
			texture_ = std::make_shared<sf::Texture>();
			texture_->loadFromFile("resources/paper_plane.png");
		}
		rect_.setTexture(texture_.get());
	}

	void update() const {
		auto v = vehicles_.lookup(vehicle_id_);
		rect_.setPosition(v.get_position());
		auto velocity = v.get_velocity();
		rect_.setRotation(angle(velocity) * 180 / pi);
	}

	void draw(sf::RenderTarget& target, sf::RenderStates states) const {
		update();
		target.draw(rect_, states);
	}
	id_t get_params_id(){
		return params_id_;
	}
private:
	const LookupContainer<Vehicle>& vehicles_;
	const id_t vehicle_id_;
	id_t params_id_;
	sf::Color colorLookup(int lookup_val);
	// The class's logical state is about the vehicle, not about the rendering resources.
	mutable sf::RectangleShape rect_;
	static std::shared_ptr<sf::Texture> texture_;
};

