#pragma once

#include <vector>

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/CircleShape.hpp>

#include "../customsteer/pathway.h"

class RenderedPolyPath : public sf::Drawable
{
public:
	explicit RenderedPolyPath(const PolyPath& path);
	void new_path(const PolyPath& path);
	void append(const PolyPath& path, const PolyPath::pathSegment& segment, bool update_cyclic = false);
	void draw(sf::RenderTarget& target, sf::RenderStates states) const;
private:
	std::vector<sf::RectangleShape> path_shading;
	std::vector<sf::RectangleShape> path_line;
	std::vector<sf::CircleShape> corner_shading;
};

