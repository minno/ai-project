#include "renderedpolypath.h"

#include <utility>
#include <cmath>

#include "../vec2.hpp"

#include <SFML/Graphics/Color.hpp>
#include <SFML/Graphics/RenderTarget.hpp>

RenderedPolyPath::RenderedPolyPath(const PolyPath& path) {
	new_path(path);
}

void RenderedPolyPath::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	for (auto& shape : corner_shading) {
		target.draw(shape, states);
	}
	for (auto& shape : path_shading) {
		target.draw(shape, states);
	}
	for (auto& shape : path_line) {
		target.draw(shape, states);
	}
}

void RenderedPolyPath::new_path(const PolyPath& path){
	corner_shading.clear();
	path_shading.clear();
	path_line.clear();
	auto& path_segments = path.get_path();
	for (auto segment : path_segments) {
		append(path, segment);
	}
}

void RenderedPolyPath::append(const PolyPath& path, const PolyPath::pathSegment& segment, bool update_cyclic){
	if(update_cyclic){
		vec2 temp_point = path_shading.back().getPosition();
		vec2 displacement = segment.origin - temp_point;
		float c_angle = std::atan2(displacement.y, displacement.x) * 180 / pi;
		path_shading.back().setSize(sf::Vector2f{magnitude(displacement), path.get_radius()*2});
		path_shading.back().setRotation(c_angle);
		path_line.back().setSize(sf::Vector2f{magnitude(displacement), 0});
		path_line.back().setRotation(c_angle);
		path_line.back().setPosition(temp_point);
	}

	sf::CircleShape circle (path.get_radius());
	circle.setFillColor(sf::Color{128, 128, 128});
	circle.setPosition(segment.origin - vec2{path.get_radius(), path.get_radius()});
	corner_shading.emplace_back(std::move(circle));

	sf::RectangleShape rect;
	rect.setOutlineThickness(0);
	rect.setFillColor(sf::Color{128, 128, 128});
	rect.setOrigin(0, path.get_radius()); // Set its "position" to be based on the middle of the short side.
	rect.setSize(sf::Vector2f{magnitude(segment.displacement), path.get_radius()*2});
	rect.setPosition(segment.origin);
	float angle = std::atan2(segment.displacement.y, segment.displacement.x) * 180 / pi;
	rect.setRotation(angle);
	path_shading.emplace_back(std::move(rect));

	sf::RectangleShape line;
	line.setOutlineThickness(1);
	line.setOutlineColor(sf::Color{0, 0, 0});
	line.setSize(sf::Vector2f{magnitude(segment.displacement), 0});
	line.setPosition(segment.origin);
	line.setRotation(angle);
	path_line.emplace_back(std::move(line));

}
