#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include "customsteer/pathway.h"

#include "drawables/renderedpolypath.h"
class Loader {
public:
	//void loadPath(char* filename = nullptr);
	float radius;
	bool cycle;
	std::string name;
	std::vector<vec2> points;
	Loader(const char* filename){
		Loader();
		try{
			loadPath(filename);
		}
		catch(...){
			std::cout << "File " << filename << "failed to load" << std::endl;
			loadFailed();
		}

	};
private:
	Loader(){
		cycle = false;
		radius = 20.0f;
		name = "no_path";
	}
	void loadFailed(){
		if(!points.empty()){
			points.clear();
		}
		cycle = false;
		radius = 20.0f;
		name = "no_path";
		//points.push_back(vec2(100,100));
		//points.push_back(vec2(500,500));

	}
	void loadPath(const char* filename);
};


void Loader::loadPath(const char* filename){

		if(!points.empty()){
			points.clear();
		}
		if(filename == nullptr){
			throw "no file provided";
		}
		std::ifstream file_stream(filename);
		std::stringstream string_stream;

		while(!file_stream.eof()){
			if(file_stream.fail()){
				throw "parses incorrectly";
			}
			std::string temp_string;
			file_stream >> temp_string;
			if(temp_string == "radius"){
				file_stream >> radius;
			}
			else if(temp_string == "name"){
				file_stream >> name;
			}
			else if(temp_string == "cycle"){
				file_stream >> cycle;
			}
			else{
				float temp_x;
				float temp_y;
				temp_x = atoi(temp_string.c_str());
				file_stream >> temp_y; //this time, we're assuming we'll always have numbers in pairs.
				points.push_back(vec2(temp_x, temp_y));
			}
			std::getline(file_stream, temp_string);
		}

		if (points.size() == 0){
			throw "no points on path";
		}
	}
